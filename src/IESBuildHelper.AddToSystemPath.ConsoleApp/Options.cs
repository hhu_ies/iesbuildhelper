﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.AddToSystemPath.ConsoleApp
{
    [CmdOptionsUsage("{appname}.exe -n name -m path1 -m path2 ...")]
    class Options
    {
        readonly List<string> _pathsToAdd = new List<string>();

        public Options()
        {
            Delimiter = ";";
            CaseSensitive = false;
        }

        [CmdOption("name|n", Purpose="variable name")]
        public string VariableName { get; set; }

        [CmdOption("delimiter|d", Purpose="delimiter")]
        public string Delimiter { get; set; }

        [CmdOption("m", Purpose="path")]
        public string Path
        {
            get
            {
                return string.Empty;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    _pathsToAdd.Add(value);
            }
        }

        [CmdSwitch("case|c", Purpose="set to case sensitive mode")]
        public bool CaseSensitive { get; set; }

        public List<string> PathsToAdd { get { return _pathsToAdd; } }
    }
}
