﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.AddToSystemPath.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new Options();
            try
            {
                CommandLineParser.Parse(args, options);
            }
            catch (CmdOptionsBindingError bindingError)
            {
                Console.Error.WriteLine("Internal error: " + bindingError.Message);
                return;
            }
            catch (CmdOptionsParserError parserError)
            {
                Console.Error.WriteLine("Error occured while reading command line options: " + parserError.Message);
                CommandLineParser.WriteUsage<Options>(Console.Error, "path2add");
                return;
            }

            var setLevel = EnvironmentVariableTarget.Machine;
            var comp = options.CaseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;
            string currentValue = Environment.GetEnvironmentVariable(options.VariableName, setLevel) ?? string.Empty;

            var currentPaths = currentValue.Split(new string[] { options.Delimiter }, StringSplitOptions.RemoveEmptyEntries).ToList();
            foreach (var pathToAdd in options.PathsToAdd)
            {
                if (!currentPaths.Exists(p => p.Equals(pathToAdd, comp)))
                    currentPaths.Add(pathToAdd);
            }

            string newValue = string.Join(options.Delimiter, currentPaths);
            Console.Write(options.VariableName);
            Console.WriteLine("=");
            Console.WriteLine(newValue);

            Environment.SetEnvironmentVariable(options.VariableName, newValue, setLevel);
        }
    }
}
