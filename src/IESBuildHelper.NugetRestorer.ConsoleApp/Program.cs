﻿using System;
using System.Diagnostics;

using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.NugetRestorer.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentDir = new Folder(new FileSystemPath(Environment.CurrentDirectory));
            var solutionFile = SolutionFile.FindInFolder(currentDir);

            if (solutionFile != null && solutionFile.Exists)
            {
                var solutionFolder = solutionFile.SolutionFolder;
                Debug.Assert(solutionFolder != null);

                var packagesCache = solutionFolder.PackagesCache;
                var nugetBootstrapper = solutionFolder.NugetBootstrapper;

                if (nugetBootstrapper.Exists)
                {
                    if (solutionFolder.SolutionLevelPackageConfig.Exists)
                        RestorePackages(nugetBootstrapper, solutionFolder.SolutionLevelPackageConfig, packagesCache);

                    foreach (var projectFile in solutionFile.GetProjectFiles())
                    {
                        Debug.Assert(projectFile != null);

                        if (projectFile.Exists)
                        {
                            var projectFolder = projectFile.ProjectFolder;
                            if (projectFolder.PackageConfig.Exists)
                                RestorePackages(nugetBootstrapper, projectFolder.PackageConfig, packagesCache);
                        }
                    }
                }
            }
        }

        static void RestorePackages(NugetBootstrapper bootstrapper, PackageConfig packageConfig, Folder packagesCache)
        {
            Console.WriteLine("Restoring packages for {0}", packageConfig.ContainingFolder.Location);

            using (var reader = bootstrapper.RestorePackages(packageConfig, packagesCache))
            {
                foreach (var line in reader.ReadLines())
                {
                    Console.Write("    ");
                    Console.WriteLine(line);
                }
            }

            Console.WriteLine();
        }
    }
}
