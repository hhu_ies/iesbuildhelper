﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using IESBuildHelper.ObjectModel;

namespace IESBuildHeper.CountTestFailures.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Options options = new Options();
            CommandLineParser.Parse(args, options);

            var errors = 0;

            var currentDir = new Folder(new FileSystemPath(Environment.CurrentDirectory));
            var solutionFile = SolutionFile.FindInFolder(currentDir);
            var solutionFolder = solutionFile.SolutionFolder;
            Folder testReportsFolder = new Folder(solutionFolder.Location.Down("tests").Down("reports").Down(options.Configuration));
            if (testReportsFolder.Exists)
            {
                foreach (var file in testReportsFolder.GetFiles().Where(f => f.Name.EndsWith(".nunit.xml", StringComparison.OrdinalIgnoreCase)))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(file.Location.ToString());
                    errors += int.Parse(doc.DocumentElement.Attributes["failures"].Value);
                }
            }

            Environment.Exit(errors);
        }
    }

    public class Options
    {
        public Options()
        {
            Configuration = "Debug";
        }

        [CmdOption("c|config", Purpose="Specify test configuration")]
        public string Configuration { get; set; }
    }
}
