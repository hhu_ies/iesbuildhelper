﻿using System;

namespace IESBuildHelper.ObjectModel
{
    public class ProjectFile : File, IEquatable<ProjectFile>
    {
        readonly ProjectFolder _projectFolder;

        public ProjectFile(FileSystemPath location)
            : base(location)
        {
            _projectFolder = new ProjectFolder(this);
        }

        public bool Equals(ProjectFile other)
        {
            return other != null && other.Location == Location;
        }

        public ProjectFolder ProjectFolder { get { return _projectFolder; } }
    }
}
