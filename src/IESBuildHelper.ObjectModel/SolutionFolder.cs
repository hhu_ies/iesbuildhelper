﻿namespace IESBuildHelper.ObjectModel
{
    public class SolutionFolder : Folder
    {
        readonly SolutionFile _solutionFile;
        readonly NugetBootstrapper _nugetBootstrapper;
        readonly Folder _packagesCache;
        readonly PackageConfig _solutionLevelPackageConfig;

        internal SolutionFolder(SolutionFile solutionFile)
            : base(solutionFile.EnsureNotNull("solutionFile").ContainingFolder.Location)
        {
            _solutionFile = solutionFile;
            _nugetBootstrapper = new NugetBootstrapper(Location.Down(".Nuget").Down("Nuget.exe"));
            _packagesCache = new Folder(Location.Down("packages"));
            _solutionLevelPackageConfig = new PackageConfig(Location.Down(".nuget").Down("packages.config"));
        }

        public SolutionFile SolutionFile { get { return _solutionFile; } }
        public NugetBootstrapper NugetBootstrapper { get { return _nugetBootstrapper; } }
        public Folder PackagesCache { get { return _packagesCache; } }
        public PackageConfig SolutionLevelPackageConfig { get { return _solutionLevelPackageConfig; } }
    }
}
