﻿using System;
using System.IO;
using System.Linq;

namespace IESBuildHelper.ObjectModel
{
    public class FileSystemPath : IEquatable<FileSystemPath>
    {
        readonly string _path;

        public FileSystemPath(string path)
        {
            _path = path.EnsureNotNull(path);
            foreach (var invalidPathNameChar in Path.GetInvalidPathChars())
                if (path.Contains(invalidPathNameChar))
                    throw new ArgumentException("Invalid path name");
        }

        public override string ToString()
        {
            return _path;
        }

        public bool Equals(FileSystemPath other)
        {
            return !ReferenceEquals(other, null) && other._path.Equals(_path, StringComparison.OrdinalIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as FileSystemPath);
        }

        public override int GetHashCode()
        {
            return _path.GetHashCode();
        }

        public static bool operator ==(FileSystemPath left, FileSystemPath right)
        {
            return
                ReferenceEquals(left, right) || (!ReferenceEquals(left, null) && left.Equals(right));
        }

        public static bool operator !=(FileSystemPath left, FileSystemPath right)
        {
            return !(left == right);
        }

        public FileSystemPath Up()
        {
            var temp = Path.GetDirectoryName(_path);
            return temp != null ? new FileSystemPath(temp) : null;
        }

        public FileSystemPath Down(string what)
        {
            return new FileSystemPath(Path.Combine(_path, what));
        }

        public bool IsRooted { get { return Path.IsPathRooted(_path); } }

        public FileSystemPath Rebase(FileSystemPath reference)
        {
            reference.EnsureNotNull("reference");
            if (IsRooted)
                return this;
            else
                return new FileSystemPath(Path.Combine(reference.ToString(), _path));
        }
    }
}
