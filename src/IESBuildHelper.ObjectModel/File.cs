﻿using System;
using System.IO;

namespace IESBuildHelper.ObjectModel
{
    public class File : IEquatable<File>
    {
        readonly FileSystemPath _path;

        public File(FileSystemPath path)
        {
            _path = path.EnsureNotNull("path");
        }

        public FileSystemPath Location { get { return _path; } }

        public Folder ContainingFolder
        {
            get
            {
                return new Folder(_path.Up());
            }
        }

        public bool Exists { get { return System.IO.File.Exists(Location.ToString()); } }
        public string Name { get { return Path.GetFileName(Location.ToString()); } }
        public string Extension { get { return Path.GetExtension(Location.ToString()); } }

        public TextReader ReadText()
        {
            return new StreamReader(new FileStream(Location.ToString(), FileMode.Open, FileAccess.Read, FileShare.Read));
        }

        public TextWriter WriteText()
        {
            return new StreamWriter(new FileStream(Location.ToString(), FileMode.OpenOrCreate, FileAccess.Write, FileShare.None));
        }

        public void TryDelete()
        {
            try
            {
                System.IO.File.Delete(Location.ToString());
            }
            catch { }
        }

        public bool Equals(File other)
        {
            return other != null && other.Location == Location;
        }

        public override bool Equals(object obj)
        {
            return obj is File && Equals((File)obj);
        }

        public override string ToString()
        {
            return Location.ToString();
        }

        public override int GetHashCode()
        {
            return Location.GetHashCode();
        }
    }
}
