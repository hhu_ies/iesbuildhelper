﻿using System;

namespace IESBuildHelper.ObjectModel
{
    public class PackageConfig : File, IEquatable<PackageConfig>
    {
        public PackageConfig(FileSystemPath location)
            : base(location) { }

        public bool Equals(PackageConfig other)
        {
            return other != null && other.Location == Location;
        }
    }
}
