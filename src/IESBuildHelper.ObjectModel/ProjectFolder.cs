﻿namespace IESBuildHelper.ObjectModel
{
    public class ProjectFolder : Folder
    {
        readonly ProjectFile _projectFile;
        readonly PackageConfig _packageConfig;

        internal ProjectFolder(ProjectFile projectFile)
            : base(projectFile.EnsureNotNull("projectFile").ContainingFolder.Location)
        {
            _projectFile = projectFile;
            _packageConfig = new PackageConfig(Location.Down("packages.config"));
        }

        public ProjectFile ProjectFile
        {
            get { return _projectFile; }
        }

        public PackageConfig PackageConfig { get { return _packageConfig; } }
    }
}
