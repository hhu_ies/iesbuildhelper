﻿using System;
using System.Collections.Generic;
using System.IO;

namespace IESBuildHelper.ObjectModel
{
    public static class Misc
    {
        public static T Setup<T>(this T instance, Action<T> action)
        {
            action.EnsureNotNull("action");
            action(instance);
            return instance;
        }

        public static T EnsureNotNull<T>(this T instance, string name)
            where T : class
        {
            if (instance == null)
                throw new ArgumentNullException(name ?? "unknown");
            else
                return instance;
        }

        public static string EnsureNotNullNorEmpty(this string instance, string name)
        {
            name = name ?? "?";
            instance.EnsureNotNull(name);
            if (instance.Length == 0)
                throw new ArgumentException(name + " cannot be empty", name);
            else
                return instance;
        }

        public static IEnumerable<string> ReadLines(this string source)
        {
            return new StringReader(source.EnsureNotNull("source")).ReadLines();
        }

        public static IEnumerable<string> ReadLines(this TextReader source)
        {
            source.EnsureNotNull("source");

            while (true)
            {
                var line = source.ReadLine();
                if (line == null)
                    break;
                else
                    yield return line;
            }
        }

        public static string RemoveHead(this string source, string head)
        {
            if (source.EnsureNotNull("source").StartsWith(head.EnsureNotNull("head")))
                return source.Substring(head.Length);
            else
                return source;
        }

        public static string RemoveTail(this string source, string tail)
        {
            if (source.EnsureNotNull("source").EndsWith(tail.EnsureNotNull("tail")))
                return source.Substring(0, source.Length - tail.Length);
            else
                return source;
        }
    }
}
