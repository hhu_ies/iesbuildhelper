﻿using System;
using System.IO;

namespace IESBuildHelper.ObjectModel
{
    public class NugetBootstrapper : File, IEquatable<NugetBootstrapper>
    {
        public NugetBootstrapper(FileSystemPath location)
            : base(location) { }

        public bool Equals(NugetBootstrapper other)
        {
            return other != null && other.Location == Location;
        }

        public TextReader RestorePackages(PackageConfig packageConfig, Folder outputDir)
        {
            packageConfig.EnsureNotNull("packageConfig");
            if (packageConfig.Exists)
            {
                outputDir.CreateFolder();

                var shell = new LocalShellService();
                var args = string.Format("install \"{0}\" /OutputDirectory \"{1}\"", packageConfig.Location, outputDir.Location);

                return shell.Run(Location.ToString(), args);
            }
            else
                return null;
        }
    }
}
