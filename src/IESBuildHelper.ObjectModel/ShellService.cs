﻿using System.Diagnostics;
using System.IO;
using System.Security;

namespace IESBuildHelper.ObjectModel
{
    public interface IShellService
    {
        TextReader Run(string command, string arguments);
        string RunToFinish(string command, string arguments);
    }

    public class LocalShellService : IShellService
    {
        readonly string _workingDir;
        readonly string _userName;
        readonly SecureString _password;

        public LocalShellService()
            : this(Directory.GetCurrentDirectory()) { }

        public LocalShellService(string workingDir)
            : this(workingDir, null, null) { }

        public LocalShellService(string workingDir, string userName, SecureString password)
        {
            _workingDir = workingDir.EnsureNotNull("workingDir");
            _userName = userName;
            _password = password;
        }

        public TextReader Run(string command, string arguments)
        {
            var startInfo = new ProcessStartInfo()
            {
                Arguments = arguments,
                FileName = command,
                RedirectStandardOutput = true,
                WorkingDirectory = _workingDir,
                UseShellExecute = false
            };

            if (_userName != null)
            {
                startInfo.UserName = _userName;
                startInfo.Password = _password;
            }

            var process = new Process();
            process.Exited += (sender, e) => process.Dispose();
            process.StartInfo = startInfo;
            process.Start();

            return process.StandardOutput;
        }

        public string RunToFinish(string command, string arguments)
        {
            var startInfo = new ProcessStartInfo()
            {
                Arguments = arguments,
                FileName = command,
                RedirectStandardOutput = true,
                WorkingDirectory = _workingDir,
                UseShellExecute = false
            };

            if (_userName != null)
            {
                startInfo.UserName = _userName;
                startInfo.Password = _password;
            }

            var process = new Process();
            process.Exited += (sender, e) => process.Dispose();
            process.StartInfo = startInfo;
            process.Start();

            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            return output;
        }
    }
}
