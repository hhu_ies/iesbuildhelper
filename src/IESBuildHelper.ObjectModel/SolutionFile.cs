﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace IESBuildHelper.ObjectModel
{
    public class SolutionFile : File, IEquatable<SolutionFile>
    {
        static readonly Regex ProjectLinePattern = new Regex(
            Patterns.SolutionFileProjectPattern,
            RegexOptions.Compiled | RegexOptions.CultureInvariant);

        readonly SolutionFolder _solutionFolder;

        public SolutionFile(FileSystemPath location)
            : base(location)
        {
            _solutionFolder = new SolutionFolder(this);
        }

        public static SolutionFile FindInFolder(Folder folder)
        {
            if (folder.Exists)
            {
                foreach (var file in folder.GetFiles())
                {
                    if (file.Extension.Equals(".sln", StringComparison.OrdinalIgnoreCase))
                        return new SolutionFile(file.Location);
                }
            }

            return null;
        }

        public IList<ProjectFile> GetProjectFiles()
        {
            var projectFiles = new List<ProjectFile>();
            using (var reader = ReadText())
            {
                while (true)
                {
                    var line = reader.ReadLine();
                    if (line == null)
                        break;

                    var match = ProjectLinePattern.Match(line);
                    if (match != null && match.Success)
                    {
                        var path = new FileSystemPath(match.Groups["ProjectFile"].Value);
                        var projectFile = new ProjectFile(path.Rebase(SolutionFolder.Location));
                        if (projectFile.Exists)
                            projectFiles.Add(projectFile);
                    }
                }
            }

            return projectFiles;
        }

        public SolutionFolder SolutionFolder { get { return _solutionFolder; } }

        public bool Equals(SolutionFile other)
        {
            return other != null && other.Location == Location;
        }
    }
}
