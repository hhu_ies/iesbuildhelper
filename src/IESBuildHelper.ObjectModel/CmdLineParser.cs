﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace IESBuildHelper.ObjectModel
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class CmdSwitchAttribute : Attribute
    {
        public string Name { get; private set; }
        public string Purpose { get; set; }

        public CmdSwitchAttribute(string name)
        {
            Name = name;
        }

        public CmdSwitchAttribute() : this(null) { }
    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class CmdOptionAttribute : Attribute
    {
        public string Name { get; private set; }
        public string Purpose { get; set; }
        public Type ConverterType { get; set; }

        public CmdOptionAttribute(string name)
        {
            Name = name;
        }

        public CmdOptionAttribute() : this(null) { }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public sealed class CmdTargetAttribute : Attribute
    {
        public string TargetName { get; private set; }
        public string Purpose { get; set; }
        public Type ConverterType { get; set; }

        public CmdTargetAttribute(string targetName)
        {
            TargetName = targetName;
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public sealed class CmdOptionsUsageAttribute : Attribute
    {
        public string Usage { get; private set; }

        public CmdOptionsUsageAttribute(string usage)
        {
            Usage = usage;
        }
    }

    public sealed class CmdTextEncoding : Attribute, ICmdOptionValueConverter
    {
        public string Decode(string text)
        {
            StringBuilder sb = new StringBuilder(text.Length);
            for (int i = 0; i < text.Length; ++i)
            {
                char c = text[i];
                if (c == '%')
                {
                    if (i < text.Length - 2)
                    {
                        string numberText = text.Substring(i + 1, 2);
                        int temp;
                        if (int.TryParse(numberText, out temp))
                        {
                            char actualChar = (char)temp;
                            sb.Append(actualChar);
                            i += 2;
                            continue;
                        }
                    }
                }

                sb.Append(c);
            }

            return sb.ToString();
        }

        public object Convert(string value)
        {
            return Decode(value);
        }
    }

    public interface ICmdOptionValueConverter
    {
        object Convert(string value);
    }

    public static class DefaultCmdOptionConverters
    {
        public class IntConverter : ICmdOptionValueConverter
        {
            public object Convert(string value) { return int.Parse(value); }
        }

        public class IntPtrConverter : ICmdOptionValueConverter
        {
            public object Convert(string value) { return new IntPtr(int.Parse(value)); }
        }

        public class EnumConverter<T> : ICmdOptionValueConverter where T : struct
        {
            public EnumConverter() { Debug.Assert(typeof(T).IsEnum); }
            public object Convert(string value) { return Enum.Parse(typeof(T), value, true); }
        }

        public class StringConverter : ICmdOptionValueConverter
        {
            public object Convert(string value) { return value; }
        }

        public class DoubleConverter : ICmdOptionValueConverter
        {
            public object Convert(string value) { return double.Parse(value); }
        }

        public class DateTimeConverter : ICmdOptionValueConverter
        {
            public object Convert(string value) { return DateTime.Parse(value); }
        }

        public class GuidConverter : ICmdOptionValueConverter
        {
            public object Convert(string value) { return new Guid(value); }
        }

        public class NullableIntConverter : ICmdOptionValueConverter
        {
            public object Convert(string value) { return (int?)int.Parse(value); }
        }

        public class NullableEnumConverter<T> : ICmdOptionValueConverter where T : struct
        {
            public NullableEnumConverter() { Debug.Assert(typeof(T).IsEnum); }
            public object Convert(string value) { return (T?)Enum.Parse(typeof(T), value, true); }
        }

        public class NullableDoubleConverter : ICmdOptionValueConverter
        {
            public object Convert(string value) { return (double?)double.Parse(value); }
        }

        public class NullableDateTimeConverter : ICmdOptionValueConverter
        {
            public object Convert(string value) { return (DateTime?)DateTime.Parse(value); }
        }

        public class NullableGuidConverter : ICmdOptionValueConverter
        {
            public object Convert(string value) { return (Guid?)(new Guid(value)); }
        }

        public static ICmdOptionValueConverter CreateConverter(Type type)
        {
            if (type == typeof(int))
                return new IntConverter();
            else if (type == typeof(IntPtr))
                return new IntPtrConverter();
            else if (type.IsEnum)
                return (ICmdOptionValueConverter)Activator.CreateInstance(typeof(EnumConverter<>).MakeGenericType(type));
            else if (type == typeof(string))
                return new StringConverter();
            else if (type == typeof(double))
                return new DoubleConverter();
            else if (type == typeof(DateTime))
                return new DateTimeConverter();
            else if (type == typeof(Guid))
                return new GuidConverter();
            else if (type == typeof(int?))
                return new NullableIntConverter();
            else if (Nullable.GetUnderlyingType(type) != null && Nullable.GetUnderlyingType(type).IsEnum)
                return (ICmdOptionValueConverter)Activator.CreateInstance(typeof(NullableEnumConverter<>).MakeGenericType(type));
            else if (type == typeof(double?))
                return new NullableDoubleConverter();
            else if (type == typeof(DateTime?))
                return new NullableDateTimeConverter();
            else if (type == typeof(Guid?))
                return new NullableGuidConverter();
            else
                return null;
        }
    }

    public static class MemberInfoExtension
    {
        public static IList<T> GetAttribute<T>(this MemberInfo member, bool inherit) where T : Attribute
        {
            return (T[])member.GetCustomAttributes(typeof(T), inherit);
        }

        public static Action<object, object> MakeSetter(this FieldInfo field)
        {
            if (field == null)
                throw new ArgumentNullException("field");

            return new Action<object, object>((target, value) => field.SetValue(target, value));
        }

        public static Action<object, object> MakeSetter(this PropertyInfo property)
        {
            if (property == null)
                throw new ArgumentNullException("property");

            var setter = property.GetSetMethod();
            if (setter == null)
                return null;

            return new Action<object, object>((target, value) => setter.Invoke(target, new object[] { value }));
        }

        public static Action<object, object> MakeSetter(this MethodInfo method)
        {
            if (method == null)
                throw new ArgumentNullException("method");

            return new Action<object, object>((target, value) => method.Invoke(target, new object[] { value }));
        }
    }

    public class CmdOptionsBindingError : Exception
    {
        public CmdOptionsBindingError(string error)
            : base(error) { }
    }

    sealed class CmdSwitch
    {
        readonly CmdSwitchAttribute _attribute;
        readonly MemberInfo _fieldOrProperty;
        readonly Action<object, bool> _setter;

        public CmdSwitch(FieldInfo fieldInfo)
        {
            if (fieldInfo == null)
                throw new ArgumentNullException("fieldInfo");

            if (fieldInfo.FieldType != typeof(bool))
                throw new ArgumentException("fieldInfo does not have type boolean");

            _fieldOrProperty = fieldInfo;
            var attr = fieldInfo.GetAttribute<CmdSwitchAttribute>(false);
            if (attr.Count < 1)
                throw new ArgumentException("field not tagged as a command line switch");

            Debug.Assert(attr.Count == 1);
            _attribute = attr[0];
            Debug.Assert(_attribute != null);

            _setter = (target, state) => fieldInfo.SetValue(target, state);
        }

        public CmdSwitch(PropertyInfo propertyInfo)
        {
            if (propertyInfo == null)
                throw new ArgumentNullException("propertyInfo");

            if (propertyInfo.PropertyType != typeof(bool))
                throw new ArgumentException("propertyInfo does not have type boolean");

            _fieldOrProperty = propertyInfo;
            var attr = propertyInfo.GetAttribute<CmdSwitchAttribute>(false);
            if (attr.Count < 1)
                throw new ArgumentNullException("property not tagged as a command line switch");

            Debug.Assert(attr.Count == 1);
            _attribute = attr[0];
            Debug.Assert(_attribute != null);

            _setter = (target, state) => propertyInfo.GetSetMethod().Invoke(target, new object[] { state });
        }

        public void SetSwitch(object target, bool state = true)
        {
            _setter(target, state);
        }

        public string MemberName { get { return _fieldOrProperty.Name; } }
        public string OptionName { get { return _attribute.Name; } }
        public CmdSwitchAttribute Attribute { get { return _attribute; } }
    }

    sealed class CmdOption
    {
        readonly CmdOptionAttribute _attribute;
        readonly MemberInfo _memberInfo;
        readonly Type _optionType;
        readonly Action<object, string> _setter;

        public CmdOption(FieldInfo fieldInfo)
        {
            if (fieldInfo == null)
                throw new ArgumentNullException("fieldInfo");

            _memberInfo = fieldInfo;
            _optionType = fieldInfo.FieldType;
            var attr = fieldInfo.GetAttribute<CmdOptionAttribute>(false);
            if (attr.Count < 1)
                throw new ArgumentException("field not tagged as a command line switch");

            Debug.Assert(attr.Count == 1);
            _attribute = attr[0];
            Debug.Assert(_attribute != null);

            ICmdOptionValueConverter converter = null;

            if (_attribute.ConverterType != null)
                converter = Activator.CreateInstance(_attribute.ConverterType) as ICmdOptionValueConverter;
            else
                converter = DefaultCmdOptionConverters.CreateConverter(OptionType);

            if (converter == null)
                throw new CmdOptionsBindingError("No type converter defined for " + OptionType.Name);

            _setter = (target, value) => fieldInfo.SetValue(target, converter.Convert(value));
        }

        public CmdOption(PropertyInfo propertyInfo)
        {
            if (propertyInfo == null)
                throw new ArgumentNullException("fieldInfo");

            _memberInfo = propertyInfo;
            _optionType = propertyInfo.PropertyType;
            var attr = propertyInfo.GetAttribute<CmdOptionAttribute>(false);
            if (attr.Count < 1)
                throw new ArgumentException("field not tagged as a command line switch");

            Debug.Assert(attr.Count == 1);
            _attribute = attr[0];
            Debug.Assert(_attribute != null);

            ICmdOptionValueConverter converter = null;

            if (_attribute.ConverterType != null)
                converter = Activator.CreateInstance(_attribute.ConverterType) as ICmdOptionValueConverter;
            else
                converter = DefaultCmdOptionConverters.CreateConverter(OptionType);

            if (converter == null)
                throw new CmdOptionsBindingError("No type converter defined for " + OptionType.Name);

            _setter = (target, value) => propertyInfo.GetSetMethod().Invoke(target, new object[] { converter.Convert(value) });
        }

        public void SetOption(object target, string value)
        {
            _setter(target, value);
        }

        public string MemberName { get { return _memberInfo.Name; } }
        public string OptionName { get { return _attribute.Name; } }
        public Type OptionType { get { return _optionType; } }
        public CmdOptionAttribute Attribute { get { return _attribute; } }
    }

    sealed class CmdTarget
    {
        readonly CmdTargetAttribute _attribute;
        readonly Type _optionType;
        readonly MemberInfo _member;
        readonly Action<object, string> _targetSetter;

        CmdTarget(Type optionType, CmdTargetAttribute attribute, PropertyInfo member)
        {
            Debug.Assert(optionType != null);
            Debug.Assert(attribute != null);
            Debug.Assert(attribute.TargetName == member.Name);
            Debug.Assert(member != null && member.DeclaringType == optionType);
            Debug.Assert(member.CanWrite);

            _optionType = optionType;
            _member = member;
            _attribute = attribute;

            Type inputType = member.PropertyType;
            Debug.Assert(inputType != null);

            ICmdOptionValueConverter converter = null;
            if (attribute.ConverterType != null)
                converter = Activator.CreateInstance(attribute.ConverterType) as ICmdOptionValueConverter;
            else
                converter = DefaultCmdOptionConverters.CreateConverter(attribute.ConverterType);

            var setter = member.MakeSetter();

            _targetSetter = (target, value) => setter(target, converter.Convert(value));
        }

        CmdTarget(Type optionType, CmdTargetAttribute attribute, FieldInfo member)
        {
            Debug.Assert(optionType != null);
            Debug.Assert(attribute != null);
            Debug.Assert(attribute.TargetName == member.Name);
            Debug.Assert(member != null && member.DeclaringType == optionType);

            _optionType = optionType;
            _member = member;
            _attribute = attribute;

            Type inputType = member.FieldType;
            Debug.Assert(inputType != null);

            ICmdOptionValueConverter converter = null;
            if (attribute.ConverterType != null)
                converter = Activator.CreateInstance(attribute.ConverterType) as ICmdOptionValueConverter;
            else
                converter = DefaultCmdOptionConverters.CreateConverter(attribute.ConverterType);

            var setter = member.MakeSetter();
            _targetSetter = (target, value) => setter(target, converter.Convert(value));
        }

        CmdTarget(Type optionType, CmdTargetAttribute attribute, MethodInfo member)
        {
            Debug.Assert(optionType != null);
            Debug.Assert(attribute != null);
            Debug.Assert(attribute.TargetName == member.Name);
            Debug.Assert(member != null && member.DeclaringType == optionType);

            _optionType = optionType;
            _member = member;
            _attribute = attribute;

            var parameters = member.GetParameters();
            Debug.Assert(parameters != null && parameters.Length == 1);

            Type inputType = parameters[0].ParameterType;
            Debug.Assert(inputType != null);

            ICmdOptionValueConverter converter = null;
            if (attribute.ConverterType != null)
                converter = Activator.CreateInstance(attribute.ConverterType) as ICmdOptionValueConverter;
            else
                converter = DefaultCmdOptionConverters.CreateConverter(attribute.ConverterType);

            var setter = member.MakeSetter();

            _targetSetter = (target, value) => setter(target, converter.Convert(value));
        }

        public static CmdTarget GetCmdTarget(Type optionType)
        {
            if (optionType == null)
                return null;

            var attributes = optionType.GetAttribute<CmdTargetAttribute>(false);
            if (attributes == null || attributes.Count == 0)
                return null;

            var attribute = attributes[0];

            var members = optionType.GetMember(attribute.TargetName);
            if (members == null || members.Length == 0)
                throw new Exception("Cannot resolve to target member " + attribute.TargetName);

            if (members.Length > 1)
            {
                List<MethodInfo> shortList = new List<MethodInfo>(1);

                foreach (var candidate in members)
                {
                    Debug.Assert(candidate is MethodInfo);
                    MethodInfo method = (MethodInfo)candidate;

                    if (method.GetParameters().Length == 1)
                        shortList.Add(method);
                }

                if (shortList.Count == 1)
                    return new CmdTarget(optionType, attribute, shortList[0]);
                else if (shortList.Count == 0)
                    throw new CmdOptionsBindingError("Cannot locate member " + attribute.TargetName + " with one parameter");
                else
                    throw new CmdOptionsBindingError("Ambiguous target member " + attribute.TargetName);
            }
            else
            {
                Debug.Assert(members.Length == 1);
                Debug.Assert(members[0] != null);
                MemberInfo member = members[0];

                if (member is PropertyInfo)
                {
                    return new CmdTarget(optionType, attribute, (PropertyInfo)member);
                }
                else if (member is MethodInfo)
                {
                    MethodInfo method = (MethodInfo)member;
                    if (method.GetParameters().Length != 1)
                        throw new CmdOptionsBindingError("Cannot locate member " + attribute.TargetName + " with one parameter");

                    return new CmdTarget(optionType, attribute, (MethodInfo)member);
                }
                else if (member is FieldInfo)
                {
                    return new CmdTarget(optionType, attribute, (FieldInfo)member);
                }
                else
                    throw new CmdOptionsBindingError("Cannot locate member " + attribute.TargetName);
            }
        }

        public void SetTarget(object options, string targetValue)
        {
            _targetSetter(options, targetValue);
        }

        public CmdTargetAttribute Attribute { get { return _attribute; } }
        public string MemberName { get { return _member == null ? null : _member.Name; } }
        public Type OptionType { get { return _optionType; } }
        public bool HasTarget { get { return _member != null; } }
    }

    sealed class CmdOptionMap
    {
        readonly Type _targetType;
        readonly CmdTarget _target;
        readonly Dictionary<string, CmdSwitch> _switches;
        readonly Dictionary<string, CmdOption> _options;
        readonly List<CmdSwitch> _uniqueSwitches;
        readonly List<CmdOption> _uniqueOptions;

        public CmdOptionMap(Type targetType)
        {
            if (targetType == null)
                throw new ArgumentNullException("targetType");

            _targetType = targetType;
            _switches = new Dictionary<string, CmdSwitch>(8);
            _options = new Dictionary<string, CmdOption>(8);
            _uniqueOptions = new List<CmdOption>(8);
            _uniqueSwitches = new List<CmdSwitch>(8);

            _target = CmdTarget.GetCmdTarget(_targetType);

            foreach (var field in _targetType.GetFields(BindingFlags.Public | BindingFlags.Instance))
            {
                if (field.GetAttribute<CmdSwitchAttribute>(false).Count > 0)
                {
                    var @switch = new CmdSwitch(field);

                    if (!string.IsNullOrEmpty(@switch.OptionName))
                    {
                        foreach (string optionName in @switch.OptionName.Split('|'))
                            _switches[@switch.OptionName] = @switch;
                    }
                    else
                        _switches[@switch.MemberName] = @switch;

                    _uniqueSwitches.Add(@switch);
                }

                if (field.GetAttribute<CmdOptionAttribute>(false).Count > 0)
                {
                    var option = new CmdOption(field);

                    if (!string.IsNullOrEmpty(option.OptionName))
                    {
                        foreach (var optionName in option.OptionName.Split('|'))
                            _options[optionName] = option;
                    }
                    else
                        _options[option.MemberName] = option;

                    _uniqueOptions.Add(option);
                }
            }

            foreach (var property in _targetType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (property.GetAttribute<CmdSwitchAttribute>(false).Count > 0)
                {
                    var @switch = new CmdSwitch(property);

                    if (!string.IsNullOrEmpty(@switch.OptionName))
                    {
                        foreach (string optionName in @switch.OptionName.Split('|'))
                            _switches[optionName] = @switch;
                    }
                    else
                        _switches[@switch.MemberName] = @switch;

                    _uniqueSwitches.Add(@switch);
                }

                if (property.GetAttribute<CmdOptionAttribute>(false).Count > 0)
                {
                    var option = new CmdOption(property);

                    if (!string.IsNullOrEmpty(option.OptionName))
                    {
                        foreach (var optionName in option.OptionName.Split('|'))
                            _options[optionName] = option;
                    }
                    else
                        _options[option.MemberName] = option;

                    _uniqueOptions.Add(option);
                }
            }
        }

        public IDictionary<string, CmdSwitch> Switches { get { return _switches; } }
        public IDictionary<string, CmdOption> Options { get { return _options; } }

        internal IList<CmdSwitch> UniqueSwitches { get { return _uniqueSwitches; } }
        internal IList<CmdOption> UniqueOptions { get { return _uniqueOptions; } }
        public CmdTarget Target { get { return _target; } }
    }

    public class CmdOptionsParserError : Exception
    {
        public CmdOptionsParserError(string error)
            : base(error) { }

        public CmdOptionsParserError(string error, Exception innerError)
            : base(string.Format("{0}\r\nCause: {1}", error, innerError.Message), innerError) { }
    }

    public sealed class CommandLineParser
    {
        enum State { ReadNext, ReadValue }

        public static void Parse<T>(string[] args, T target)
        {
            CmdOptionMap optionMap = new CmdOptionMap(typeof(T));
            State state = State.ReadNext;

            CmdSwitch currentSwitch = null;
            CmdOption currentOption = null;

            foreach (string arg in args)
            {
                switch (state)
                {
                    case State.ReadNext:
                        {
                            if (arg.StartsWith("-") || arg.StartsWith("/"))
                            {
                                currentSwitch = null;
                                currentOption = null;

                                if (optionMap.Switches.TryGetValue(arg.Substring(1), out currentSwitch))
                                {
                                    SetSwitch<T>(target, currentSwitch);
                                    state = State.ReadNext;
                                }
                                else if (optionMap.Options.TryGetValue(arg.Substring(1), out currentOption))
                                {
                                    state = State.ReadValue;
                                }
                                else
                                    throw new CmdOptionsParserError("Unknown command line switch");
                            }
                            else
                            {
                                if (optionMap.Target == null)
                                    throw new CmdOptionsParserError("Expecting - or /");
                                else
                                    optionMap.Target.SetTarget(target, arg);
                            }
                        }
                        break;

                    case State.ReadValue:
                        {
                            string value = arg;
                            if (arg.Length > 2 && arg.StartsWith("\"") && arg.EndsWith("\""))
                                value = arg.Substring(1, arg.Length - 2);

                            Debug.Assert(currentOption != null);
                            SetOption<T>(target, currentOption, value);

                            state = State.ReadNext;
                        }
                        break;

                    default:
                        return;
                }
            }

            if (state == State.ReadValue)
                throw new CmdOptionsParserError("Unexpected end of argument list");
        }

        static void SetSwitch<T>(T target, CmdSwitch currentSwitch)
        {
            try
            {
                currentSwitch.SetSwitch(target);
            }
            catch (Exception ex)
            {
                throw new CmdOptionsParserError("Error occured while setting switch " + currentSwitch.OptionName, ex);
            }
        }

        static void SetOption<T>(T target, CmdOption currentOption, string value)
        {
            try
            {
                currentOption.SetOption(target, value);
            }
            catch (Exception ex)
            {
                throw new CmdOptionsParserError("Error occured while setting option " + currentOption.OptionName, ex);
            }
        }

        public static void WriteUsage<T>(System.IO.TextWriter sink, string applicationName, bool writeOptionsDetail = false)
        {
            sink.WriteLine("Usage: ");

            Type type = typeof(T);
            var usageAttributes = type.GetAttribute<CmdOptionsUsageAttribute>(false);
            foreach (var usage in usageAttributes)
                if (!string.IsNullOrEmpty(usage.Usage))
                {
                    sink.WriteLine("\t" + usage.Usage.Replace("{appname}", applicationName));
                }

            if (writeOptionsDetail)
            {
                sink.WriteLine();

                var map = new CmdOptionMap(type);
                if (map.Target != null)
                {
                    sink.WriteLine("Target: {0}", map.Target.Attribute.Purpose);
                }

                sink.WriteLine();
                sink.WriteLine("Switches:");
                foreach (var @switch in map.UniqueSwitches)
                {
                    sink.WriteLine("\t-{0}", @switch.Attribute.Name.Replace("|", " -"));
                    sink.WriteLine("\t{0}", @switch.Attribute.Purpose);
                    sink.WriteLine();
                }

                sink.WriteLine();
                sink.WriteLine("Options:");
                foreach (var option in map.UniqueOptions)
                {
                    sink.WriteLine("\t-{0}", option.Attribute.Name.Replace("|", " -"));
                    sink.WriteLine("\t{0}", option.Attribute.Purpose);
                    sink.WriteLine();
                }
            }
        }
    }
}