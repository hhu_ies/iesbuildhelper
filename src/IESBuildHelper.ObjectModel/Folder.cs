﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace IESBuildHelper.ObjectModel
{
    public class Folder : IEquatable<Folder>
    {
        readonly FileSystemPath _location;

        public Folder(FileSystemPath location)
        {
            _location = location.EnsureNotNull("location");
        }

        public bool Exists { get { return Directory.Exists(Location.ToString()); } }

        public IEnumerable<Folder> GetSubfolders()
        {
            return Directory.GetDirectories(Location.ToString()).Select(f => new Folder(new FileSystemPath(f)));
        }

        public Folder GetSubfolder(string name)
        {
            var test = new Folder(Location.Down(name.EnsureNotNullNorEmpty("name")));
            if (test.Exists)
                return test;
            else
                return null;
        }

        public IEnumerable<File> GetFiles()
        {
            return Directory.GetFiles(Location.ToString()).Select(f => new File(new FileSystemPath(f)));
        }

        public File GetFile(string name)
        {
            var test = new File(Location.Down(name.EnsureNotNullNorEmpty("name")));
            if (test.Exists)
                return test;
            else
                return null;
        }

        public FileSystemPath Location { get { return _location; } }
        public string Name { get { return Path.GetFileName(Location.ToString()); } }

        public bool Equals(Folder other)
        {
            return other != null && Location == other.Location;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Folder);
        }

        public override string ToString()
        {
            return Location.ToString();
        }

        public override int GetHashCode()
        {
            return Location.GetHashCode();
        }

        public void CreateFolder()
        {
            Directory.CreateDirectory(Location.ToString());
        }

        public void TryRemove()
        {
            Directory.Delete(Location.ToString(), true);
        }
    }
}
