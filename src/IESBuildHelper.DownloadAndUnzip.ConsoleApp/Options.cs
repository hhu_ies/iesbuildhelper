﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.DownloadAndUnzip.ConsoleApp
{
    [CmdOptionsUsage("{appname}.exe -s source -d destination")]
    class Options
    {
        [CmdOption("s|source", Purpose = "download URL", ConverterType = typeof(UriConverter))]
        public Uri Source { get; set; }

        [CmdOption("d|destination", Purpose = "destination file", ConverterType = typeof(FileSystemPathConverter))]
        public FileSystemPath Destination { get; set; }
    }

    class UriConverter : ICmdOptionValueConverter
    {
        public object Convert(string value)
        {
            return new Uri(value);
        }
    }

    class FileSystemPathConverter : ICmdOptionValueConverter
    {
        public object Convert(string value)
        {
            return new FileSystemPath(value);
        }
    }
}
