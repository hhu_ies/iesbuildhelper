﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;
using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.DownloadAndUnzip.ConsoleApp
{
    public static class ZipFileExtensions
    {
        public static Folder UnzipTo(this ZipFile zipFile, FileSystemPath folderPath)
        {
            zipFile.EnsureNotNull("zipFile");
            folderPath.EnsureNotNull("folder");
            var folder = new Folder(folderPath);
            if (folder.Exists)
            {
                folder.TryRemove();
                folder.CreateFolder();
            }

            zipFile.ExtractAll(folderPath.ToString(), ExtractExistingFileAction.OverwriteSilently);
            return new Folder(folderPath);
        }
    }
}
