﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.DownloadAndUnzip.ConsoleApp
{
    public class TempFile: File, IDisposable
    {
        public TempFile()
            : base(new FileSystemPath(System.IO.Path.GetTempFileName())) { }

        public void Dispose()
        {
            if (Exists)
                TryDelete();
        }
    }
}
