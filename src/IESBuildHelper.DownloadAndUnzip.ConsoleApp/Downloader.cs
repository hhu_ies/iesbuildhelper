﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.DownloadAndUnzip.ConsoleApp
{
    public class Downloader
    {
        public bool DownloadToFile(Uri sourceLocation, FileSystemPath destLocation)
        {
            try
            {
                var file = new File(destLocation);
                if (file.Exists)
                    file.TryDelete();

                using (var client = new WebClient())
                {
                    client.DownloadFile(sourceLocation, destLocation.ToString());
                }
                    
                return true;
            }
            catch { return false; }
        }
    }
}
