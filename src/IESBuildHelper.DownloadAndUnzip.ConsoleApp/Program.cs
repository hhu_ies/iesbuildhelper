﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.DownloadAndUnzip.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new Options();
            try
            {
                CommandLineParser.Parse(args, options);
            }
            catch (CmdOptionsBindingError bindingError)
            {
                Console.Error.WriteLine("Internal error: " + bindingError.Message);
                return;
            }
            catch (CmdOptionsParserError parserError)
            {
                Console.Error.WriteLine("Error occured while reading command line options: " + parserError.Message);
                CommandLineParser.WriteUsage<Options>(Console.Error, "DAUZ");
                return;
            }

            var downloader = new Downloader();
            using (var tempFile = new TempFile())
            {
                downloader.DownloadToFile(options.Source, tempFile.Location);
                var zipFile = new Ionic.Zip.ZipFile(tempFile.ToString());
                zipFile.UnzipTo(options.Destination);
            }
        }
    }


}
