﻿using System;
using System.Collections.Generic;
using System.Configuration;

using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.GenerateXUnitTestScripts.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Options options = new Options();
            CommandLineParser.Parse(args, options);

            var currentDir = new Folder(new FileSystemPath(Environment.CurrentDirectory));
            var solutionFile = SolutionFile.FindInFolder(currentDir);

            if (solutionFile != null && solutionFile.Exists)
            {
                var tests = new List<string>();

                foreach (var projectFile in solutionFile.GetProjectFiles())
                {
                    if (projectFile.Exists && projectFile.ContainingFolder.Name.EndsWith(".Tests"))
                    {
                        var projectFolder = projectFile.ProjectFolder;
                        var testName = projectFolder.Name.RemoveTail(".Tests");
                        var testXMLFile = new File(projectFolder.Location.Down("tests.run.xml"));
                        testXMLFile.TryDelete();

                        using (var writer = testXMLFile.WriteText())
                        {
                            writer.Write(
                                Resources.TestXMLTemplate
                                    .Replace("{{testName}}", testName)
                                    .Replace("{{xunitDir}}", options.XunitDir)
                                    .Replace("{{xunitToolsDir}}", options.XunitToolsDir));
                        }

                        Console.WriteLine("Created test script {0}", testXMLFile.Location);
                        tests.Add(testName);
                    }
                }

                var testFolder = new Folder(solutionFile.SolutionFolder.Location.Down("tests"));
                testFolder.CreateFolder();

                var masterRunTestScript = new File(testFolder.Location.Down("run-unittests.xml"));
                masterRunTestScript.TryDelete();
                using (var writer = masterRunTestScript.WriteText())
                {
                    string scriptContent = Resources.MasterRunTestScript;
                    string insertionPoint = "<!--insert tests here-->";
                    foreach (var testName in tests)
                    {
                        scriptContent = scriptContent.Replace(
                            insertionPoint,
                            string.Format("<testProject Include=\"$(srcDir)\\{0}.Tests\\tests.run.xml\"/>{1}", testName, insertionPoint));
                    }

                    writer.Write(scriptContent);
                }

                Console.WriteLine("Created master test script {0}", masterRunTestScript.Location);
            }
        }
    }



    public class Options
    {
        public Options()
        {
            XunitDir = ConfigurationManager.AppSettings["xunitDir"];
            XunitToolsDir = ConfigurationManager.AppSettings["xunitToolsDir"];
        }

        [CmdOption("lib", Purpose="xunit library directory")]
        public string XunitDir { get; set; }

        [CmdOption("tools", Purpose="xunit tools directory")]
        public string XunitToolsDir { get; set; }
    }
}
