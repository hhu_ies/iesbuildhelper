﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.IncrementBuildInfo.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Options options = new Options();
                CommandLineParser.Parse(args, options);

                if (options.Help)
                    CommandLineParser.WriteUsage<Options>(Console.Out, "incbuild", true);
                else
                {
                    options.Normalise();
                    var file = new File(options.BuildInfoPath);
                    if (file.Exists)
                    {
                        BuildInfo buildInfo;
                        using (var reader = file.ReadText())
                            buildInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<BuildInfo>(reader.ReadToEnd());

                        buildInfo.BuildNumber++;
                        buildInfo.BuildTime = DateTime.Now;
                        if (!string.IsNullOrEmpty(options.BuildConfig))
                            buildInfo.BuildConfig = options.BuildConfig;

                        file.TryDelete();
                        using (var writer = file.WriteText())
                            writer.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(buildInfo));
                    }
                    else
                    {
                        var buildInfo = new BuildInfo();
                        buildInfo.BuildNumber = 1;
                        buildInfo.BuildTime = DateTime.Now;
                        if (!string.IsNullOrEmpty(options.BuildConfig))
                            buildInfo.BuildConfig = options.BuildConfig;
                        else
                            buildInfo.BuildConfig = "Local";

                        using (var writer = file.WriteText())
                            writer.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(buildInfo));
                    }
                }
            }
            catch (Exception error)
            {
                Console.Error.WriteLine(error.Message);
            }
        }
    }

    class PathConverter : ICmdOptionValueConverter
    {
        public object Convert(string value)
        {
            return new FileSystemPath(value);
        }
    }

    [CmdOptionsUsage("{appname}.exe -i (build info file) -config (build config) [-n (build number)]")]
    [CmdOptionsUsage("{appname}.exe -h")]
    public class Options
    {
        [CmdSwitch("h", Purpose="Show help")]
        public bool Help { get; set; }

        [CmdOption("i|info", Purpose="Specify build info file", ConverterType=typeof(PathConverter))]
        public FileSystemPath BuildInfoPath { get; set; }

        [CmdOption("c|config", Purpose="Specify build config")]
        public string BuildConfig { get; set; }

        [CmdOption("n|buildnumber", Purpose = "Specify build number")]
        public int? BuildNumber { get; set; }

        public void Normalise()
        {
            if (BuildInfoPath == null)
            {
                var currentDir = new Folder(new FileSystemPath(Environment.CurrentDirectory));
                var solutionFile = SolutionFile.FindInFolder(currentDir);
                var solutionFolder = solutionFile.SolutionFolder;
                var deployFolder = solutionFolder.GetSubfolder("deploy");
                BuildInfoPath = deployFolder.Location.Down("buildinfo." + (BuildConfig ?? "Local") + ".json");
            }
        }
    }

    public class BuildInfo
    {
        public int BuildNumber { get; set; }
        public string BuildConfig { get; set; }
        public DateTime BuildTime { get; set; }
    }
}
