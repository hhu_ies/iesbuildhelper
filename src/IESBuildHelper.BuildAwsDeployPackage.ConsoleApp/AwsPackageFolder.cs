﻿using System.Xml;

using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.BuildAwsDeployPackage.ConsoleApp
{
    public class AwsPackageFolder : Folder
    {
        protected AwsPackageFolder(Folder folder)
            : base(folder.EnsureNotNull("folder").Location) { }

        public static AwsPackageFolder FromDeployFolder(DeployFolder deployFolder)
        {
            deployFolder.EnsureNotNull("deployFolder");
            if (deployFolder.Exists)
            {
                var test = new Folder(deployFolder.Location.Down("aws-package"));
                test.CreateFolder();

                return new AwsPackageFolder(test);
            }

            return null;
        }

        public File InitParametersXml()
        {
            var file = new File(Location.Down("parameters.xml"));
            using (var writer = file.WriteText())
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("parameters").Setup(parameters =>
                {
                    parameters.AppendChild(doc.CreateElement("parameter").Setup(parameter =>
                    {
                        parameter.SetAttribute("name", "IIS Web Application Name");
                        parameter.SetAttribute("defaultValue", "Default Web Site/");
                        parameter.SetAttribute("tags", "IisApp");
                    }));
                }));

                doc.Save(writer);
            }

            return file;
        }

        public File InitManifestXml(WebsitesFolder websitesFolder, InstallScript installScript)
        {
            websitesFolder.EnsureNotNull("websitesFolder");
            installScript.EnsureNotNull("installScript");

            var file = new File(Location.Down("manifest.xml"));
            using (var writer = file.WriteText())
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("siteManifest").Setup(siteManifest =>
                {
                    siteManifest.AppendChild(doc.CreateElement("contentPath").Setup(contentPath =>
                    {
                        contentPath.SetAttribute("path", websitesFolder.Location.ToString());
                    }));

                    siteManifest.AppendChild(doc.CreateElement("runCommand").Setup(runCommand =>
                    {
                        runCommand.SetAttribute("path", installScript.Location.ToString());
                        runCommand.SetAttribute("waitInterval", "30000");
                    }));
                }));

                doc.Save(writer);
            }

            return file;
        }

        public File InitBuildScript(File manifestXml, File parametersXml, string packageName)
        {
            var file = new File(Location.Down("make-aws-package.cmd"));
            using (var writer = file.WriteText())
            {
                writer.WriteLine("SET PATH=%PATH%;" + @"C:\Program Files\IIS\Microsoft Web Deploy V3\");
                writer.WriteLine("msdeploy -verb:sync -source:manifest=\"{0}\" -declareParamfile=\"{1}\" -dest:package=\"{2}\"",
                    manifestXml.Location, parametersXml.Location, Location.Down(packageName));
            }

            return file;
        }
    }
}
