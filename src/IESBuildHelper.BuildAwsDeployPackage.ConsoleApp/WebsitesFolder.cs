﻿using System.Collections.Generic;

using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.BuildAwsDeployPackage.ConsoleApp
{
    public class WebsitesFolder : Folder
    {
        readonly IList<DeployableWebsiteCache> _websiteCaches;

        protected WebsitesFolder(FileSystemPath location)
            : base(location)
        {
            _websiteCaches = DeployableWebsiteCache.FromWebsitesFolder(this);
        }

        public IList<DeployableWebsiteCache> WebsiteCaches { get { return _websiteCaches; } }

        public static WebsitesFolder FromDeployFolder(DeployFolder deployFolder)
        {
            var test = deployFolder.GetSubfolder("websites");
            if (test != null && test.Exists)
                return new WebsitesFolder(test.Location);
            else
                return null;
        }
    }
}
