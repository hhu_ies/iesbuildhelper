﻿
using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.BuildAwsDeployPackage.ConsoleApp
{
    public class InstallScript : File
    {
        protected InstallScript(FileSystemPath location)
            : base(location) { }

        public static InstallScript Create(WebsitesFolder websitesFolder)
        {
            websitesFolder.EnsureNotNull("websitesFolder");

            var script = new InstallScript(websitesFolder.Location.Down("install.cmd"));

            using (var writer = script.WriteText())
            {
                writer.WriteLine("SET PATH=%PATH%;" + @"C:\Program Files\IIS\Microsoft Web Deploy V3\");

                foreach (var website in websitesFolder.WebsiteCaches)
                {
                    writer.WriteLine("CD \"{0}\"", website.Location);
                    
                    if (website.InstallScript != null)
                    {
                        writer.WriteLine("CALL \"{0}\"", website.InstallScript.Location);
                    }
                    else
                    {
                        writer.WriteLine(
                            "msdeploy.exe -verb:sync -source:package=\"{0}\" -dest:auto -setParamFile:\"{1}\"",
                            website.DefaultPackage.Location,
                            website.AwsSetParametersXml.Location);
                    }

                    writer.WriteLine();
                }
            }

            return script;
        }
    }
}
