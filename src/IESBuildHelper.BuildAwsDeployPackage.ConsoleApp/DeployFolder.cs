﻿
using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.BuildAwsDeployPackage.ConsoleApp
{
    public class DeployFolder : Folder
    {
        readonly WebsitesFolder _websitesFolder;
        readonly AwsPackageFolder _awsPackageFolder;

        protected DeployFolder(FileSystemPath location)
            : base(location)
        {
            _websitesFolder = WebsitesFolder.FromDeployFolder(this);
            _awsPackageFolder = AwsPackageFolder.FromDeployFolder(this);
        }

        public WebsitesFolder WebsitesFolder { get { return _websitesFolder; } }
        public AwsPackageFolder AwsPackageFolder { get { return _awsPackageFolder; } }

        public static DeployFolder FromSolutionFolder(SolutionFolder solutionFolder)
        {
            solutionFolder.EnsureNotNull("solutionFolder");
            var deployFolder = new DeployFolder(solutionFolder.Location.Down("deploy"));
            if (deployFolder.Exists)
                return deployFolder;
            else
                return null;
        }
    }
}
