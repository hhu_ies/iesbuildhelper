﻿using System;
using System.Collections.Generic;
using System.Linq;

using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.BuildAwsDeployPackage.ConsoleApp
{
    public class DeployableWebsiteCache : Folder
    {
        readonly File _defaultPackage;
        readonly File _awsSetParametersXml;
        readonly File _installScript;

        protected DeployableWebsiteCache(FileSystemPath location, File installScript)
            :base(location)
        {
            _defaultPackage = null;
            _awsSetParametersXml = null;
            _installScript = installScript.EnsureNotNull("installScript");
        }

        protected DeployableWebsiteCache(FileSystemPath location, File defaultPackage, File awsSetParametersXml)
            : base(location)
        {
            _defaultPackage = defaultPackage.EnsureNotNull("defaultPackage");
            _awsSetParametersXml = awsSetParametersXml.EnsureNotNull("awsSetParametersXml");
        }

        public File DefaultPackage { get { return _defaultPackage; } }
        public File AwsSetParametersXml { get { return _awsSetParametersXml; } }
        public File InstallScript { get { return _installScript; } }

        public static DeployableWebsiteCache FromFolder(Folder folder)
        {
            if (folder != null && folder.Exists)
            {
                var awsSetParametersXml = folder.GetFile("AwsSetParameters.xml");
                var installScript = folder.GetFile("installScript.cmd");
                var packagesFolder = folder.GetSubfolder("packages");

                if (installScript != null)
                    return new DeployableWebsiteCache(folder.Location, installScript);
                else if (awsSetParametersXml != null)
                {
                    if (packagesFolder != null && packagesFolder.Exists)
                    {
                        File defaultPackage = null;
                        var files = packagesFolder.GetFiles();
                        var zipFiles = files.Where(f => f.Extension.Equals(".zip", StringComparison.OrdinalIgnoreCase)).ToArray();
                        if (zipFiles.Length == 1)
                            defaultPackage = zipFiles[0];
                        else if (zipFiles.Length > 1)
                        {
                            defaultPackage = zipFiles.SingleOrDefault(f => f.Name.Equals(folder.Name + ".zip", StringComparison.OrdinalIgnoreCase));
                            if (defaultPackage == null)
                                defaultPackage = zipFiles.SingleOrDefault(f => f.Name.Equals("website.zip", StringComparison.OrdinalIgnoreCase));

                            if (defaultPackage == null)
                                defaultPackage = zipFiles[0];
                        }

                        if (defaultPackage != null && defaultPackage.Exists)
                            return new DeployableWebsiteCache(folder.Location, defaultPackage, awsSetParametersXml);
                    }
                }
            }

            return null;
        }

        public static IList<DeployableWebsiteCache> FromWebsitesFolder(WebsitesFolder websitesFolder)
        {
            websitesFolder.EnsureNotNull("websitesFolder");
            if (websitesFolder == null)
                return new DeployableWebsiteCache[] { };
            else
                return websitesFolder.GetSubfolders().Select(dir => FromFolder(dir)).Where(cache => cache != null).OrderBy(c => c.Name).ToList();
        }
    }
}
