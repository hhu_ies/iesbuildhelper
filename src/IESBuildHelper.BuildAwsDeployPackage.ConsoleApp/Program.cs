﻿using System;
using IESBuildHelper.ObjectModel;

namespace IESBuildHelper.BuildAwsDeployPackage.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentDir = new Folder(new FileSystemPath(Environment.CurrentDirectory));
            var solutionFile = SolutionFile.FindInFolder(currentDir);
            var solutionFolder = solutionFile.SolutionFolder;
            var deployFolder = DeployFolder.FromSolutionFolder(solutionFolder);
            var parametersXml = deployFolder.AwsPackageFolder.InitParametersXml();
            var manifestXml = deployFolder.AwsPackageFolder.InitManifestXml(deployFolder.WebsitesFolder, InstallScript.Create(deployFolder.WebsitesFolder));
            var buildScript = deployFolder.AwsPackageFolder.InitBuildScript(manifestXml, parametersXml, "aws-deploy-package.zip");

            var shell = new LocalShellService(deployFolder.AwsPackageFolder.Location.ToString());
            shell.RunToFinish(buildScript.Location.ToString(), string.Empty);
        }
    }
}
